FROM node:current

ADD dist .

ENTRYPOINT [ "node", "app.js" ]